iTunes API Search List

   I used the MVVM architecture for the app because it promotes decoupling and makes the application
easier to read because of the separation of concerns. I also used Dagger2 for dependency injection and Retrofit for
retrieving data from the API. 

  The SharedPreferences have been used to store the date when user last used the app and
it stores the last visited screen by the user. I used this because it is an easy way to store small amounts of data.

Source: https://affiliate.itunes.apple.com/resources/documentation/itunes-store-web-service-search-api/#searching