package com.yra.movieslist.data.repository

import com.yra.movieslist.data.models.MovieList
import com.yra.movieslist.data.service.MovieService
import com.yra.movieslist.di.annotations.ApplicationScope
import io.reactivex.Single
import javax.inject.Inject

/**
 * Created by Yra on 2019-12-13
 * Movieslist
 */
@ApplicationScope
class MovieRepository @Inject constructor(private val movieService: MovieService) {

    fun getListMovies(params: Map<String, String>) : Single<MovieList> = movieService.getList(params)

    fun getMovieDetails(trackId: Long) : Single<MovieList> = movieService.getMovieDetails(trackId)
}