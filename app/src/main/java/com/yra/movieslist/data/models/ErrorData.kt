package com.yra.movieslist.data.models

/**
 * Created by Yra on 2019-12-13
 * Movieslist
 */
class ErrorData(message: String = "", errorCode: String = "", throwable: Throwable? = null) {
    var message : String = message
    var errorCode : String = errorCode
    var throwable : Throwable? = throwable
}