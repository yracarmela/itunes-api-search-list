package com.yra.movieslist.data.models

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.databind.annotation.JsonSerialize

/**
 * Created by Yra on 2019-12-14
 * Movieslist
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
data class MovieList(val resultCount: Long = 0L, val results: Array<Movies> = arrayOf(Movies())) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as MovieList

        if (resultCount != other.resultCount) return false
        if (!results.contentEquals(other.results)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = resultCount.hashCode()
        result = 31 * result + results.contentHashCode()
        return result
    }
}