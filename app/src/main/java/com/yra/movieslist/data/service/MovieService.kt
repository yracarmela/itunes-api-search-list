package com.yra.movieslist.data.service

import com.yra.movieslist.data.models.MovieList
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query
import retrofit2.http.QueryMap

/**
 * Created by Yra on 2019-12-12
 * Movieslist
 */
interface MovieService {

    @GET("search")
    fun getList(@QueryMap params: Map<String, String>) : Single<MovieList>

    @GET("lookup")
    fun getMovieDetails(@Query("id") trackId: Long) : Single<MovieList>
}