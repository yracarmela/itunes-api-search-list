package com.yra.movieslist.data.models

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import java.io.Serializable

/**
 * Created by Yra on 2019-12-13
 * Movieslist
 */
@JsonIgnoreProperties(ignoreUnknown = true)
data class Movies(val wrapperType: String = "", val kind: String = "", val artistId: Long = 0L, val collectionId: Long = 0L,
                  val trackId: Long = 0L, val artistName: String = "", val collectionName: String = "", val trackName: String = "", val previewUrl: String = "",
                  val artworkUrl30: String = "", val artworkUrl60: String = "", val artworkUrl100: String = "", val trackTimeMillis: Long = 0L,
                  val trackPrice: Double = 0.00, val currency: String = "", val primaryGenreName: String = "", val contentAdvisoryRating: String = "",
                  val shortDescription: String = "", val longDescription: String = "") : Serializable