package com.yra.movieslist.data

import android.content.Context
import android.content.SharedPreferences
import com.yra.movieslist.util.ApiConfig
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by Yra on 2019-12-12
 * Movieslist
 */
@Singleton
class PreferenceProvider @Inject constructor(var mContext: Context) {

    private var mPrefs: SharedPreferences = mContext.getSharedPreferences(ApiConfig.MOVIE_PREFERENCE,
        Context.MODE_PRIVATE)

    fun stampLastVisitDate(lastVisitTimestamp: Long) {
        mPrefs.edit().putLong(LAST_VISITED, lastVisitTimestamp).apply()
    }

    fun getLastVisitDate(): Long {
        return mPrefs.getLong(LAST_VISITED, 0L)
    }

    fun saveLastMovieId(movieId: Long) {
        mPrefs.edit().putLong(MOVIE_ID, movieId).apply()
    }

    fun getLastMovieId(): Long {
        return mPrefs.getLong(MOVIE_ID, 0L)
    }

    fun saveLastState(exitFromList: Boolean, exitFromDetails: Boolean) {
        mPrefs.edit().putBoolean(EXITED_FROM_LIST, exitFromList).apply()
        mPrefs.edit().putBoolean(EXITED_FROM_DETAILS, exitFromDetails).apply()
    }

    fun isExitedFromList(): Boolean {
        return mPrefs.getBoolean(EXITED_FROM_LIST, true)
    }

    fun isExitedFromDetails(): Boolean {
        return mPrefs.getBoolean(EXITED_FROM_DETAILS, false)
    }

    companion object {
        private const val LAST_VISITED = "lastVisited"
        private const val MOVIE_ID = "movieId"
        private const val EXITED_FROM_DETAILS = "exitedFromDetails"
        private const val EXITED_FROM_LIST = "exitedFromList"
    }
}