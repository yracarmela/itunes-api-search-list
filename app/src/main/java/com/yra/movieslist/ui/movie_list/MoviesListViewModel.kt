package com.yra.movieslist.ui.movie_list

import androidx.lifecycle.MutableLiveData
import com.yra.movieslist.common.BaseViewModel
import com.yra.movieslist.data.PreferenceProvider
import com.yra.movieslist.data.models.Movies
import com.yra.movieslist.data.repository.MovieRepository
import com.yra.movieslist.schedulers.SchedulerProvider
import com.yra.movieslist.util.Util
import java.util.*
import javax.inject.Inject
import kotlin.collections.HashMap

/**
 * Created by Yra on 2019-12-13
 * Movieslist
 */
class MoviesListViewModel @Inject constructor(private val schedulerProvider: SchedulerProvider,
                                              private val movieRepository: MovieRepository,
                                              private val mPrefs: PreferenceProvider) : BaseViewModel() {

    val listMovies = MutableLiveData<List<Movies>>()
    val showLoading = MutableLiveData<Boolean>()
    val movieDetailId = MutableLiveData<Long>()

    var movieList: List<Movies>? = null

    fun getListMovies() {
        showLoading.value = true
        val params = HashMap<String, String>()
        params[SEARCH_TERM] = "star"
        params[COUNTRY] = "au"
        params[MEDIA_TYPE] = "movie"
        compositeDisposable.add(
            movieRepository.getListMovies(params)
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .subscribe({
                    movieList = it.results.toList()
                    listMovies.value = movieList
                    stampLastVisit()
                    showLoading.value = false
                }, { onErrorResult(TAG, "getListMovies()", it) })
        )
    }

    private fun stampLastVisit() {
        val lastVisitTimestamp = Date().time
        mPrefs.stampLastVisitDate(lastVisitTimestamp)
    }

    fun getLastVisitDate(): String? {
        val lastVisitTimestamp = mPrefs.getLastVisitDate()
        val dateVisit = Date(lastVisitTimestamp)

        return if (lastVisitTimestamp > 0L) Util.formatDate(dateVisit) else null
    }

    fun saveState() {
        mPrefs.saveLastState(exitFromList = true, exitFromDetails = false)
        mPrefs.saveLastMovieId(0L)
    }

    fun getSavedState() {
        val isExitFromList = mPrefs.isExitedFromList()
        val isExitFromDetails = mPrefs.isExitedFromDetails()
        if (isExitFromDetails)
            movieDetailId.value = mPrefs.getLastMovieId()
    }

    fun getSelectedMovie(movieId: Long) : Movies? {
        movieList?.let {
            for (m in it) {
                if (movieId == m.trackId)
                    return m
            }
        }
        return null
    }

    companion object {
        private const val TAG = "MoviesListViewModel"
        private const val SEARCH_TERM = "term"
        private const val COUNTRY = "country"
        private const val MEDIA_TYPE = "media"
    }
}