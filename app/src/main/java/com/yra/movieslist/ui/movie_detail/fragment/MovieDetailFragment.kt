package com.yra.movieslist.ui.movie_detail.fragment

import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.MediaController
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.yra.movieslist.R
import com.yra.movieslist.common.BaseFragment
import com.yra.movieslist.data.models.Movies
import kotlinx.android.synthetic.main.activity_item_detail.*
import kotlinx.android.synthetic.main.item_detail.view.*
import javax.inject.Inject

/**
 * A fragment representing a single Item detail screen.
 * This fragment is either contained in a [MoviesListActivity]
 * in two-pane mode (on tablets) or a [MovieDetailActivity]
 * on handsets.
 */
class MovieDetailFragment : BaseFragment() {

    @Inject lateinit var viewModelFactory: ViewModelProvider.Factory
    private lateinit var movieDetailViewModel: MovieDetailFragmentViewModel

    private var mMovie: Movies? = null
    private lateinit var rootView: View

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        movieDetailViewModel = ViewModelProviders.of(this, viewModelFactory)
            .get(MovieDetailFragmentViewModel::class.java)

        arguments?.let {
            if (it.containsKey(MOVIE_SELECTED)) {
                // Load the content specified by the fragment
                // arguments. In a real-world scenario, use a Loader
                // to load content from a content provider.
                mMovie = it.getSerializable(MOVIE_SELECTED) as Movies?

                activity?.toolbar_layout?.title = mMovie?.trackName
            }
        }
        setUp()
    }

    override fun setUp() {
        movieDetailViewModel.apply {
            movieDetails.observe(this@MovieDetailFragment,
                Observer<Movies> {
                    mMovie = it
                    showMovieDetails()
                })
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        rootView = inflater.inflate(R.layout.item_detail, container, false)
        showMovieDetails()
        return rootView
    }

    private fun showMovieDetails() {
        mMovie?.let {
            rootView.tvMovieDesc?.text = it.longDescription

            val mediaController = MediaController(context)
            val uri: Uri = Uri.parse(it.previewUrl)
            rootView.videoView?.setVideoURI(uri)
            mediaController.setAnchorView(rootView.videoView)
            rootView.videoView?.setMediaController(mediaController)
            rootView.videoView?.start()
        }
    }

    override fun onPause() {
        super.onPause()
        movieDetailViewModel.saveState(mMovie?.trackId ?: 0L)
    }

    companion object {
        const val MOVIE_SELECTED = "movie_selected"
    }
}
