package com.yra.movieslist.ui.movie_list

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.yra.movieslist.R
import com.yra.movieslist.common.BaseActivity
import com.yra.movieslist.data.models.Movies
import com.yra.movieslist.ui.movie_detail.activity.MovieDetailActivity
import com.yra.movieslist.ui.movie_detail.fragment.MovieDetailFragment
import com.yra.movieslist.ui.movie_list.adapter.MovieAdapter
import kotlinx.android.synthetic.main.activity_movie_list.*
import kotlinx.android.synthetic.main.item_list.*
import javax.inject.Inject

/**
 * An activity representing a list of Pings. This activity
 * has different presentations for handset and tablet-size devices. On
 * handsets, the activity presents a list of items, which when touched,
 * lead to a [MovieDetailActivity] representing
 * item details. On tablets, the activity presents the list of items and
 * item details side-by-side using two vertical panes.
 */
class MoviesListActivity : BaseActivity() {

    @Inject lateinit var viewModelFactory: ViewModelProvider.Factory
    private lateinit var moviesListViewModel: MoviesListViewModel

    /**
     * Whether or not the activity is in two-pane mode, i.e. running on a tablet
     * device.
     */
    private var twoPane: Boolean = false
    private var layoutManager: LinearLayoutManager? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movie_list)

        moviesListViewModel = ViewModelProviders.of(this, viewModelFactory)
            .get(MoviesListViewModel::class.java)

        setSupportActionBar(toolbar)
        toolbar.title = title
        layoutManager = LinearLayoutManager(this)

        if (item_detail_container != null) {
            // The detail container view will be present only in the
            // large-screen layouts (res/values-w900dp).
            // If this view is present, then the
            // activity should be in two-pane mode.
            twoPane = true
        }

        setUp()
    }

    override fun setUp() {
        moviesListViewModel.getListMovies()
        moviesListViewModel.apply {
            listMovies.observe(this@MoviesListActivity,
                Observer<List<Movies>> { setupRecyclerView(it) })
            showLoading.observe(this@MoviesListActivity,
                Observer<Boolean> { showLoadingProgress(it) })
            movieDetailId.observe(this@MoviesListActivity,
                Observer<Long> { if (it > 0L) startDetailActivity(it) })
        }
        val lastVisitTime = moviesListViewModel.getLastVisitDate()
        lastVisitTime?.let {
            val lastVisitText = "Last visited on: $it"
            tvLastVisit?.text = lastVisitText
            tvLastVisit?.visibility = View.VISIBLE
        }
    }

    private fun startDetailActivity(movieId: Long) {
        val intent = Intent(this, MovieDetailActivity::class.java)
        val selectedMovie = moviesListViewModel.getSelectedMovie(movieId)
        intent.putExtra(MovieDetailFragment.MOVIE_SELECTED, selectedMovie)
        startActivity(intent)
    }

    private fun setupRecyclerView(movieList: List<Movies>) {
        item_list?.layoutManager = layoutManager
        item_list?.adapter = MovieAdapter(
            this,
            movieList,
            twoPane
        )
        moviesListViewModel.getSavedState()
    }

    override fun onPause() {
        super.onPause()
        moviesListViewModel.saveState()
    }

    private fun showLoadingProgress(isLoading: Boolean) {
        tvLastVisit?.visibility = if (isLoading) View.GONE else View.VISIBLE
        item_list?.visibility = if (isLoading) View.GONE else View.VISIBLE
        progressView?.visibility = if (isLoading) View.VISIBLE else View.GONE
    }

}
