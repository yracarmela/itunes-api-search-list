package com.yra.movieslist.ui.movie_detail.fragment

import com.yra.movieslist.data.PreferenceProvider
import com.yra.movieslist.data.repository.MovieRepository
import com.yra.movieslist.schedulers.SchedulerProvider
import dagger.Module
import dagger.Provides

/**
 * Created by Yra on 2019-12-14
 * Movieslist
 */
@Module
class MovieDetailFragmentModule {

    @Provides
    fun provideViewModel(preferenceProvider: PreferenceProvider, movieRepository: MovieRepository,
                         schedulerProvider: SchedulerProvider)
            = MovieDetailFragmentViewModel(preferenceProvider, movieRepository, schedulerProvider)
}