package com.yra.movieslist.ui.movie_detail.activity

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.yra.movieslist.R
import com.yra.movieslist.common.BaseActivity
import com.yra.movieslist.data.models.Movies
import com.yra.movieslist.ui.movie_detail.fragment.MovieDetailFragment
import com.yra.movieslist.ui.movie_list.MoviesListActivity
import kotlinx.android.synthetic.main.activity_item_detail.*
import javax.inject.Inject

/**
 * An activity representing a single Item detail screen. This
 * activity is only used on narrow width devices. On tablet-size devices,
 * item details are presented side-by-side with a list of items
 * in a [MoviesListActivity].
 */
class MovieDetailActivity : BaseActivity() {

    @Inject lateinit var viewModelFactory: ViewModelProvider.Factory
    private lateinit var movieDetailViewModel: MovieDetailActivityViewModel

    var mMovie: Movies? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_item_detail)

        movieDetailViewModel = ViewModelProviders.of(this, viewModelFactory)
            .get(MovieDetailActivityViewModel::class.java)

        setSupportActionBar(detail_toolbar)

        // Show the Up button in the action bar.
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        // savedInstanceState is non-null when there is fragment state
        // saved from previous configurations of this activity
        // (e.g. when rotating the screen from portrait to landscape).
        // In this case, the fragment will automatically be re-added
        // to its container so we don't need to manually add it.
        // For more information, see the Fragments API guide at:
        //
        // http://developer.android.com/guide/components/fragments.html
        //
        if (savedInstanceState == null) {
            // Create the detail fragment and add it to the activity
            // using a fragment transaction.
            val fragment = MovieDetailFragment().apply {
                arguments = Bundle().apply {
                    mMovie = intent.getSerializableExtra(MovieDetailFragment.MOVIE_SELECTED) as Movies
                    putSerializable(
                        MovieDetailFragment.MOVIE_SELECTED,
                        intent.getSerializableExtra(MovieDetailFragment.MOVIE_SELECTED)
                    )
                }
            }

            supportFragmentManager.beginTransaction()
                .add(R.id.item_detail_container, fragment)
                .commit()
        }
        setUp()
    }

    override fun setUp() {
//        if (ivMovieImage != null)
//            Glide.with(this)
//                .load(mMovie?.artworkUrl100)
//                .into(ivMovieImage)
    }

    override fun onOptionsItemSelected(item: MenuItem) =
        when (item.itemId) {
            android.R.id.home -> {
                // This ID represents the Home or Up button. In the case of this
                // activity, the Up button is shown. For
                // more details, see the Navigation pattern on Android Design:
                //
                // http://developer.android.com/design/patterns/navigation.html#up-vs-back

                navigateUpTo(Intent(this, MoviesListActivity::class.java))
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
}
