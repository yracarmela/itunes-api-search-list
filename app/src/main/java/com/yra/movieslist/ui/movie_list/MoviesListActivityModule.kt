package com.yra.movieslist.ui.movie_list

import com.yra.movieslist.data.PreferenceProvider
import com.yra.movieslist.data.repository.MovieRepository
import com.yra.movieslist.schedulers.SchedulerProvider
import dagger.Module
import dagger.Provides

/**
 * Created by Yra on 2019-12-13
 * Movieslist
 * This module links the activity with its viewmodel and provides
 * the dependencies needed by the viewmodel.
 */
@Module
class MoviesListActivityModule {

    @Provides
    fun provideViewModel(schedulerProvider: SchedulerProvider, movieRepository: MovieRepository, preferenceProvider: PreferenceProvider)
            = MoviesListViewModel(schedulerProvider, movieRepository, preferenceProvider)
}