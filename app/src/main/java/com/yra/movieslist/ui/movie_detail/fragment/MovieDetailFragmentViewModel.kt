package com.yra.movieslist.ui.movie_detail.fragment

import androidx.lifecycle.MutableLiveData
import com.yra.movieslist.common.BaseViewModel
import com.yra.movieslist.data.PreferenceProvider
import com.yra.movieslist.data.models.Movies
import com.yra.movieslist.data.repository.MovieRepository
import com.yra.movieslist.schedulers.SchedulerProvider
import javax.inject.Inject

/**
 * Created by Yra on 2019-12-14
 * Movieslist
 */
class MovieDetailFragmentViewModel @Inject constructor(private val mPrefs: PreferenceProvider,
                                                       private val movieRepository: MovieRepository,
                                                       private val schedulerProvider: SchedulerProvider) : BaseViewModel() {

    val movieDetails = MutableLiveData<Movies>()
    fun saveState(movieId: Long) {
        mPrefs.saveLastMovieId(movieId)
        mPrefs.saveLastState(exitFromList = false, exitFromDetails = true)
    }

//    fun getMovieDetails(movieId: Long) {
//        compositeDisposable.add(
//            movieRepository.getMovieDetails(movieId)
//                .subscribeOn(schedulerProvider.io())
//                .observeOn(schedulerProvider.ui())
//                .subscribe({
//                    movieDetails.value = it.results[0]
//                }, { onErrorResult(TAG, "getMovieDetails()", it)})
//        )
//    }



    companion object {
        private const val TAG = "MovieDetailFragmentViewModel"
    }
}