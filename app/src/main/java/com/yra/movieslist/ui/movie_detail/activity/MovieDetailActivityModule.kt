package com.yra.movieslist.ui.movie_detail.activity

import dagger.Module
import dagger.Provides

/**
 * Created by Yra on 2019-12-14
 * Movieslist
 */
@Module
class MovieDetailActivityModule {

    @Provides
    fun provideViewModel()
            = MovieDetailActivityViewModel()
}