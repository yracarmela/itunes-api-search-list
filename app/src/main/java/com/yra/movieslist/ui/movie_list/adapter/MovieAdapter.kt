package com.yra.movieslist.ui.movie_list.adapter

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.yra.movieslist.R
import com.yra.movieslist.data.models.Movies
import com.yra.movieslist.ui.movie_detail.activity.MovieDetailActivity
import com.yra.movieslist.ui.movie_detail.fragment.MovieDetailFragment
import com.yra.movieslist.ui.movie_list.MoviesListActivity
import kotlinx.android.synthetic.main.item_list_content.view.*

/**
 * Created by Yra on 2019-12-14
 * Movieslist
 * This adapter displays the list of movies from iTunes API.
 */
class MovieAdapter(private val parentActivity: MoviesListActivity,
                   private val mData: List<Movies>,
                   private val twoPane: Boolean) : RecyclerView.Adapter<MovieAdapter.MovieViewHolder>() {

    private val onClickListener: View.OnClickListener

    init {
        onClickListener = View.OnClickListener { v ->
            val item = v.tag as Movies

            if (twoPane) {
                val fragment = MovieDetailFragment().apply {
                    arguments = Bundle().apply {
                        putSerializable(MovieDetailFragment.MOVIE_SELECTED, item)
                    }
                }
                parentActivity.supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.item_detail_container, fragment)
                    .commit()
            } else {
                val intent = Intent(v.context, MovieDetailActivity::class.java).apply {
                    putExtra(MovieDetailFragment.MOVIE_SELECTED, item)
                }
                v.context.startActivity(intent)
            }
        }
    }

    override fun getItemId(position: Int): Long {
        return mData[position].trackId
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_list_content, parent, false)
        return MovieViewHolder(view)
    }

    override fun getItemCount() = mData.size

    override fun onBindViewHolder(holder: MovieViewHolder, position: Int) {
        val movie = mData[holder.adapterPosition]

        Glide.with(parentActivity)
            .load(movie.artworkUrl100)
            .placeholder(R.drawable.ic_movie_placeholder)
            .into(holder.ivMovie)

        holder.tvTrackName.text = movie.trackName
        holder.tvGenre.text = movie.primaryGenreName
        val trackPrice = "A$ ${movie.trackPrice}"
        holder.tvPrice.text = trackPrice
        holder.tvShortDesc.text = movie.shortDescription
        with(holder.itemView) {
            tag = movie
            setOnClickListener(onClickListener)
        }
    }


    inner class MovieViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val ivMovie: ImageView = view.ivMovie
        val tvTrackName: TextView = view.tvTrackName
        val tvGenre: TextView = view.tvGenre
        val tvPrice: TextView = view.tvPrice
        val tvShortDesc: TextView = view.tvShortDesc
    }
}