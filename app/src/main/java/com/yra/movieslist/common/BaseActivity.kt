package com.yra.movieslist.common

import android.view.View
import android.widget.Toast
import androidx.annotation.StringRes
import androidx.lifecycle.Observer
import com.google.android.material.snackbar.Snackbar
import com.yra.movieslist.R
import dagger.android.support.DaggerAppCompatActivity

/**
 * Created by Yra on 2019-12-13
 * Movieslist
 */
abstract class BaseActivity : DaggerAppCompatActivity() {

    fun observerLoader(viewModel: BaseViewModel, loaderView: View) {
        viewModel.loader.observe(this, Observer {
            loaderView.visibility = if (it != null && it) View.VISIBLE else View.GONE
        })
    }

    fun onError(@StringRes message: Int) {
        onError(resources.getString(message))
    }

    fun onError(message : String) {
        createSnackBar(message)
    }

    private fun createSnackBar(message : String) {
        val snackBar : Snackbar = Snackbar.make(findViewById(android.R.id.content),
            message, Snackbar.LENGTH_SHORT)
//        val view : View = snackBar.view
//        val textView : TextView = view.findViewById(android.support.design.R.id.snackbar_text)
//        val textView : TextView = view.findViewById(android.support)
//        textView.setTextColor(ContextCompat.getColor(this, R.color.white))
        snackBar.show()
    }

    fun showMessage(@StringRes message : Int) {
        showMessage(resources.getString(message))
    }

    fun showMessage(message : String?) {
        if (message != null)
            createToast(message)
        else
            createToast(resources.getString(R.string.generic_error_message))
    }

    private fun createToast(message : String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    abstract fun setUp()
}