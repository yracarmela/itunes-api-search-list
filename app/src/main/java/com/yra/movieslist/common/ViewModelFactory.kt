package com.yra.movieslist.common

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.yra.movieslist.di.annotations.ApplicationScope
import javax.inject.Inject
import javax.inject.Provider

/**
 * Created by Yra on 2019-12-13
 * Movieslist
 */
@ApplicationScope
class ViewModelFactory @Inject constructor(private val creators : Map<Class<out ViewModel>, @JvmSuppressWildcards Provider<ViewModel>>) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        var creator : Provider<out ViewModel>? = creators[modelClass]

        if (creator == null) {
            for ((key, value) in creators) {
                if (modelClass.isAssignableFrom(key)) {
                    creator = value
                    break
                }
            }
        }

        requireNotNull(creator) { "Unknown model class $modelClass" }

        try {
            return creator.get() as T
        } catch (e : Exception) {
            throw RuntimeException(e)
        }
    }

}