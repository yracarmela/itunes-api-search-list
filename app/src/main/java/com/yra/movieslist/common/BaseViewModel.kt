package com.yra.movieslist.common

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.yra.movieslist.data.models.ErrorData
import io.reactivex.disposables.CompositeDisposable

/**
 * Created by Yra on 2019-12-13
 * Movieslist
 */
abstract class BaseViewModel : ViewModel() {

    val loader : MutableLiveData<Boolean> = MutableLiveData()
    val error : MutableLiveData<ErrorData?> = MutableLiveData()
    var compositeDisposable : CompositeDisposable = CompositeDisposable()

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }

    fun onErrorResult(callingClass: String, method: String, throwable: Throwable?) {
        Log.e(callingClass, method, throwable)
    }
}