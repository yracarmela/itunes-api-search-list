package com.yra.movieslist.common

import android.widget.Toast
import androidx.annotation.StringRes
import com.google.android.material.snackbar.Snackbar
import com.yra.movieslist.R
import dagger.android.support.DaggerFragment


/**
 * Created by Yra on 2019-12-13
 * Movieslist
 */
abstract class BaseFragment : DaggerFragment() {

    fun onError(@StringRes message : Int) {
        onError(resources.getString(message))
    }

    fun onError(message : String) {
        createSnackBar(message)
    }

    private fun createSnackBar(message : String) {
        var snackBar : Snackbar? = null
        activity?.let {
            snackBar = Snackbar.make(
                it.findViewById(android.R.id.content),
                message, Snackbar.LENGTH_SHORT)
        }
//        val view : View? = snackBar?.view

//        val textView : TextView? = view?.findViewById(android.support.design.R.id.snackbar_text)
//        context?.let {
//            textView?.setTextColor(ContextCompat.getColor(it, R.color.white))
//        }

        snackBar?.show()
    }

    fun showMessage(@StringRes message : Int) {
        showMessage(resources.getString(message))
    }

    fun showMessage(message : String?) {
        if (message != null)
            createToast(message)
        else
            createToast(resources.getString(R.string.generic_error_message))
    }

    private fun createToast(message : String) {
        Toast.makeText(activity!!, message, Toast.LENGTH_SHORT).show()
    }

    abstract fun setUp()
}