package com.yra.movieslist.util

import com.yra.movieslist.BuildConfig

/**
 * Created by Yra on 2019-12-12
 * Movieslist
 */
object ApiConfig {

    const val MOVIE_PREFERENCE = "MoviePreference"
    const val BASE_URL: String = BuildConfig.API_HOST
    const val MAX_CACHE_SIZE : Long = 10 * 1024 * 1024 // 10 MB
    const val CACHE_DIRECTORY = "http"
    const val MAX_AGE = 10
    const val PRAGMA_HEADER = "Pragma"
    const val CACHE_CONTROL_HEADER = "Cache-Control"
}