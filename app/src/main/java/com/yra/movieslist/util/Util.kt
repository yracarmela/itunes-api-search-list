package com.yra.movieslist.util

import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by Yra on 2019-12-14
 * Movieslist
 */
object Util {

    fun formatDate(date: Date, format: String? = null): String {
        var format2 = format
        if (format2 == null)
            format2 = "MMM dd, yyyy k:m:s"
        val dateFormat = SimpleDateFormat(format2, Locale.getDefault())
        return dateFormat.format(date.time)
    }
}