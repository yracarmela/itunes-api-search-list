package com.yra.movieslist.di.module

import android.content.Context
import com.yra.movieslist.MoviesApp
import com.yra.movieslist.data.PreferenceProvider
import com.yra.movieslist.di.annotations.ApplicationScope
import com.yra.movieslist.schedulers.SchedulerProvider
import dagger.Module
import dagger.Provides

/**
 * Created by Yra on 2019-12-11
 * Movieslist
 */
@Module(includes = [NetworkServiceModule::class])
class AppModule {

    @Provides
    @ApplicationScope
    fun provideContext(application: MoviesApp) : Context
            = application

    @Provides
    fun provideSchedulerProvider()
            = SchedulerProvider()

    @Provides
    fun providePreferences(context: Context) : PreferenceProvider
            = PreferenceProvider(context)
}