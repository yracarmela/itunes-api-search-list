package com.yra.movieslist.di.annotations

import javax.inject.Scope

/**
 * Created by Yra on 2019-12-11
 * Movieslist
 */
@Scope
@MustBeDocumented
@Retention(AnnotationRetention.RUNTIME)
annotation class PreferenceInfoScope