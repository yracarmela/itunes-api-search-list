package com.yra.movieslist.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.yra.movieslist.common.ViewModelFactory
import com.yra.movieslist.di.annotations.ViewModelKey
import com.yra.movieslist.ui.movie_detail.activity.MovieDetailActivityViewModel
import com.yra.movieslist.ui.movie_detail.fragment.MovieDetailFragmentViewModel
import com.yra.movieslist.ui.movie_list.MoviesListViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

/**
 * Created by Yra on 2019-12-11
 * Movieslist
 * This module binds the activity/fragment with the ViewModel factory.
 */
@Module
abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(MoviesListViewModel::class)
    abstract fun bindMoviesListViewModel(moviesListViewModel: MoviesListViewModel) : ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MovieDetailActivityViewModel::class)
    abstract fun bindMovieDetailActivityViewModel(movieDetailActivityViewModel: MovieDetailActivityViewModel) : ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MovieDetailFragmentViewModel::class)
    abstract fun bindMovieDetailFragmentViewModel(movieDetailFragmentViewModel: MovieDetailFragmentViewModel) : ViewModel

    @Binds
    abstract fun bindViewModelFactory(viewModelFactory: ViewModelFactory) : ViewModelProvider.Factory

}