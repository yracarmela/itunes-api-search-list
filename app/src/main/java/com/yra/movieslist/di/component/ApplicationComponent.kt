package com.yra.movieslist.di.component

import com.yra.movieslist.MoviesApp
import com.yra.movieslist.di.annotations.ApplicationScope
import com.yra.movieslist.di.module.ActivityBuilderModule
import com.yra.movieslist.di.module.AppModule
import com.yra.movieslist.di.module.ViewModelModule
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule

/**
 * Created by Yra on 2019-12-11
 * Movieslist
 */
@ApplicationScope
@Component(modules = arrayOf(AndroidSupportInjectionModule::class, AppModule::class,
    ViewModelModule::class, ActivityBuilderModule::class))
interface ApplicationComponent : AndroidInjector<MoviesApp> {

    @Component.Builder
    abstract class Builder : AndroidInjector.Builder<MoviesApp>()
}