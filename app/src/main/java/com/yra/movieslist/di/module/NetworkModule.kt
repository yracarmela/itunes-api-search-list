package com.yra.movieslist.di.module

import android.content.Context
import com.fasterxml.jackson.databind.ObjectMapper
import com.yra.movieslist.di.annotations.ApplicationScope
import com.yra.movieslist.util.ApiConfig
import dagger.Module
import dagger.Provides
import io.reactivex.schedulers.Schedulers
import okhttp3.Cache
import okhttp3.CacheControl
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.jackson.JacksonConverterFactory
import java.io.File
import java.util.concurrent.TimeUnit
import javax.inject.Named

/**
 * Created by Yra on 2019-12-11
 * Movieslist
 */
@Module
class NetworkModule {

    @Provides
    @ApplicationScope
    fun provideCache(@ApplicationScope context: Context) : Cache {
        return Cache(File(context.cacheDir, ApiConfig.CACHE_DIRECTORY), ApiConfig.MAX_CACHE_SIZE)
    }

    @Provides
    @Named(LOGGING_INTERCEPTOR)
    @ApplicationScope
    fun provideHttpLoggingInterceptor() : HttpLoggingInterceptor {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        return interceptor
    }

    @Provides
    @Named(CACHE_INTERCEPTOR)
    @ApplicationScope
    fun provideCacheInterceptor() : Interceptor {
        return Interceptor { chain ->
            val request = chain.request()
            val response = chain.proceed(request)

            val cacheControl: CacheControl =
                CacheControl.Builder()
                    .maxAge(ApiConfig.MAX_AGE, TimeUnit.SECONDS)
                    .build()

            response.newBuilder()
                .removeHeader(ApiConfig.PRAGMA_HEADER)
                .removeHeader(ApiConfig.CACHE_CONTROL_HEADER)
                .header(ApiConfig.CACHE_CONTROL_HEADER, cacheControl.toString())
                .build()
        }
    }

    @Provides
    @ApplicationScope
    fun provideOkHttpClient(cache: Cache,
                            @Named(CACHE_INTERCEPTOR) cacheInterceptor: Interceptor,
                            @Named(LOGGING_INTERCEPTOR) loggingInterceptor: HttpLoggingInterceptor) : OkHttpClient {
        return OkHttpClient.Builder()
            .cache(cache)
            .addNetworkInterceptor(cacheInterceptor)
            .addInterceptor(loggingInterceptor)
            .build()
    }

    @Provides
    @ApplicationScope
    fun provideRetrofit(okHttpClient: OkHttpClient) : Retrofit {
        return Retrofit.Builder()
            .baseUrl(ApiConfig.BASE_URL)
            .client(okHttpClient)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
            .addConverterFactory(JacksonConverterFactory.create(ObjectMapper()))
            .build()
    }

    companion object {
        private const val CACHE_INTERCEPTOR = "cacheInterceptor"
        private const val LOGGING_INTERCEPTOR = "loggingInterceptor"
    }
}