package com.yra.movieslist.di.module

import com.yra.movieslist.di.annotations.ActivityScope
import com.yra.movieslist.ui.movie_detail.activity.MovieDetailActivity
import com.yra.movieslist.ui.movie_detail.activity.MovieDetailActivityModule
import com.yra.movieslist.ui.movie_detail.fragment.MovieDetailFragment
import com.yra.movieslist.ui.movie_detail.fragment.MovieDetailFragmentModule
import com.yra.movieslist.ui.movie_list.MoviesListActivity
import com.yra.movieslist.ui.movie_list.MoviesListActivityModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created by Yra on 2019-12-11
 * Movieslist
 * This module injects the activity/fragment with their corresponding
 * activity module.
 */
@Module
abstract class ActivityBuilderModule {

    @ActivityScope
    @ContributesAndroidInjector(modules = [MoviesListActivityModule::class])
    abstract fun bindMoviesListActivity() : MoviesListActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [MovieDetailActivityModule::class])
    abstract fun bindMovieDetailActivity() : MovieDetailActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [MovieDetailFragmentModule::class])
    abstract fun bindMovieDetailFragment() : MovieDetailFragment
}