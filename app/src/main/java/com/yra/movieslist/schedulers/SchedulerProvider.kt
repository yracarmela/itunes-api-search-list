package com.yra.movieslist.schedulers

import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * Created by Yra on 2019-12-14
 * Movieslist
 */
class SchedulerProvider {

    fun ui() : Scheduler {
        return AndroidSchedulers.mainThread()
    }

    fun io() : Scheduler {
        return Schedulers.io()
    }

    fun computation() : Scheduler {
        return Schedulers.computation()
    }
}